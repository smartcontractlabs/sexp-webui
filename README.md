This is the SEXP web UI. Intended for use with https://gitlab.com/smartcontractlabs/sexp-binary-options . A live testnet instance is running at https://testnet.sexp.exchange

## Dependencies
To build this a NodeJS environment is needed. Node version v14.16.0 is known to work, but any later version also likely works.

## API dependencies
The explorer TzKT (https://github.com/baking-bad/tzkt), specifically the public instance running at https://api.florencenet.tzkt.io/. Both provided by Baking Bad.

A public Tezos node, currently the florencenet one running at https://florencenet.smartpy.io/, provided by SmartPy.

Price graphs, provided by Trading View (https://www.tradingview.com). Their availability has no effect on the rest of the app.

## Build Instructions
1. Clone the repo

```
git clone https://gitlab.com/smartcontractlabs/sexp-webui.git
cd ./sexp-webui
```

2. Install dependencies
```
npm install
```

3. Run Sapper's export -- this will produce a folder of static assets
```
npm run export
```

4. Host this using any webserver, e.g. npx serve or sirv-cli
```
npm run sirv
```
