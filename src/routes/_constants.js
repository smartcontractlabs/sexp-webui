// florencenet
export const KUSD_ADDR = 'KT1LWN8m9JMAzeV4nyfzpVigchr84NWGQbUw';
export const HARBINGER_ADDR = 'KT1SUP27JhX24Kvr11oUdWswk7FnCW78ZyUn';
export const EXAMPLE_MARKET_ADDR = 'KT1DfWU3JHJ4SxjpnkDtcTbVcTqWK498Krbw';
export const DEPLOYER_ADDR = 'KT1Ws9UhhUYphe81A8hUeGzMny8cmrd53BeT';
export const MARKET_FACTORY_ADDR = 'KT1REVMff3m8cJasSiAsjL59xsa1ybPgFyV1';
export const RPC_URL = 'https://florencenet.smartpy.io/';
export const TZKT_URL = 'https://api.florencenet.tzkt.io/v1';

// assets available on harbinger
export const assetPairs = [
    "BTC-USD",
    "ETH-USD",
    "XTZ-USD",
    "LINK-USD",
    "COMP-USD",
    "KNC-USD",
    "REP-USD",
    "ZRX-USD",
    "DAI-USDC"
]

export const phases = ['bidding', 'trading', 'maturity'];
export const assetDecimals = {
    "BTC-USD": 2,
    "ETH-USD": 2,
    "XTZ-USD": 4,
    "LINK-USD": 3,
    "COMP-USD": 2,
    "KNC-USD": 3,
    "REP-USD": 3,
    "ZRX-USD": 4,
    "DAI-USDC": 4,
};