import { BigNumber } from "bignumber.js";
import { get, derived, readable, writable } from 'svelte/store';
import { RPC_URL, TZKT_URL, KUSD_ADDR, HARBINGER_ADDR, MARKET_FACTORY_ADDR } from './_constants';
import posts from './markets/_posts';

// export const pick = (obj, whitelist) => Object.fromEntries(Object.entries(obj).filter(([key]) => whitelist.includes(key)));
const delay = (ms, arg) => {
    return new Promise(resolve => setTimeout(() => resolve(arg), ms));
};


export const kusdAddr = writable(KUSD_ADDR);
export const harbingerAddr = writable(HARBINGER_ADDR);

export let initBeacon;
export const beacon = new Promise(res => initBeacon = res)
    .then(x => {console.log("initBeacon: ", x); return x});

export const getBeaconAddress = () => JSON.parse(localStorage.getItem('beacon:accounts'))?.[0].address

export const beaconDeletePermissions = (beacon) => {
    localStorage.clear();
    new beacon.DAppClient({ name: 'SEXP Binary Options' }).setActiveAccount();
    userAddr.set(new Promise(() => {}))
}

export const beaconRequestPermissions = (beacon) => {
    console.log("beacon: ", beacon); // not a promise
    // reset permissions first, in case user deleted them in wallet
    beaconDeletePermissions(beacon);
    new beacon.DAppClient({ name: 'SEXP Binary Options' }).setActiveAccount()
    .then(() => {
        const client = new beacon.DAppClient({ name: 'SEXP Binary Options' });
		client.setColorMode('dark');
        return client.requestPermissions({
            network: {
                type: "florencenet",
                rpcUrl: RPC_URL
            },
            scopes: [
                "operation_request",
                "sign"
            ]
        });
    })
    .then(res => {
        console.log("permission response: ", res);
        if (res.scopes.some(permission => permission === "operation_request")) {
            const addr = getBeaconAddress()
            console.log("beacon permissions granted for address", addr);
            userAddr.set(Promise.resolve(addr));

            // update all dependent stuff once, dont wait until next block
            updateKusdBalance(get(userAddr));
            for (const [addr, market] of get(marketsMap).entries()) {
                market.getters.getLongs();
                market.getters.getShorts();
            };
        }
        return;
    })
    .catch(e => console.error("requestPermission error: ", e));
};

export const enableUpdater = writable(true);

const deploymentsTimestamp = writable(undefined);

export const timestamp = writable(0);

export const updater = () => {
    if(get(enableUpdater) === false) return delay(2000).then(updater);
    return fetch(`${TZKT_URL}/head`)
    .then(r => r.json())
    .then(bh => bh.timestamp)
    .then(ts => new Date(ts).valueOf())
    .then(ts => {
        const current = get(timestamp);
        console.log("ts: ", ts);
        console.log("current: ", current);
        if (current > ts) {
            console.error("RPC block header timestamp is stale: ", ts);
            return delay(2000).then(updater);
        }
        else if (current === ts) {
            // we are on the current block, try again soon
            console.log("latest block is still the same, timestamp: ", ts)
            return delay(2000).then(updater);
        }
        else {
            console.log("RPC node has new block available, timestamp: ", ts);
            timestamp.set(ts);

            // fetch updates here
            for (const [addr, market] of get(marketsMap).entries()) {
                const d = get(market.stores.data);
                if (d !== "loading") {
                    if (d.phase === 0 || (d.phase === 1 && d.trading_end < ts)) {
                        market.getters.getData();
                        if (getBeaconAddress() !== undefined) {
                            market.getters.getLongs();
                            market.getters.getShorts();
                        }
                    }
                }
            };
            if (getBeaconAddress() !== undefined) {
                updateKusdBalance(get(userAddr));
            }
            getDeploymentsFromTzkt()
            .then(arr => {
                // prioritize loading the market currently being viewed
                if (typeof window !== "undefined") {
                    const currentAddress = (new URL(document.location))?.searchParams.get('contract');
                    if (currentAddress) {
                        arr.unshift(currentAddress);
                    }
                }                
                return arr;
            })
            .then(addresses => addresses.map(addr => {
                // add newly available contracts
                // on first run this includes contracts that were not available at prerender (build) time
                if (get(marketsMap).has(addr) === false) {
                    console.log("spawning market that was not preloaded: ", addr);
                    addMarket(addr);
                }
                return;
            }));
            

            const timeUntilNextBlock = (ts + 30 * 1000) - Date.now();
            console.log("next block in: ", timeUntilNextBlock);
            return delay(timeUntilNextBlock).then(updater);
        }
    });
};


export const userAddr = writable(new Promise(() => {}));


export const kusdUserBalance = writable("loading");

// TzKT get current kusd balance 
// https://api.florencenet.tzkt.io/v1/contracts/KT1LWN8m9JMAzeV4nyfzpVigchr84NWGQbUw/bigmaps/balances/keys/tz1WB6v4YZ9Ze5Kwdw5x5daJLHyGQDWVD1Qo
const updateKusdBalance = (address) => address
    .then(addr => fetch(`${TZKT_URL}/contracts/${KUSD_ADDR}/bigmaps/balances/keys/${addr}`))
    .then(r => r.text())
    .then(rt => rt ? JSON.parse(rt) : {})
    .then(entry => {
        // kusd doesnt remove keys with 0 balance (the boolean 'active' won't be false in TzKT)
        if (entry?.value?.balance === undefined) {
            return new BigNumber(0);
        } else {
            return new BigNumber(entry.value.balance);
        }
    })
    .then(balance => {
        if (balance !== get(kusdUserBalance)) {
            console.log("kusd balance ready: ", balance);
            kusdUserBalance.set(balance);
        }
    });

// export const kusdUserBalance = writable(new Promise(() => {}));
// const updateKusdBalance = (address) => Promise.all([address, kusdBalances])
//     .then(([addr, b]) => b.get(addr))
//     .then(entry => entry.balance)
//     .then(b => b === undefined ? 0 : b)
//     .then(balance => {
//         kusdUserBalance.set(Promise.resolve(balance));
//     });
// 
// equivalent to above
// uses $ reactive syntax, works only in .svelte files
// let kusdUserBalance = new Promise(() => {});
// $: Promise.all([$userAddr, $kusdBalances, $timestamp]).then(([addr, kb, ts]) => kb.get(addr)).then(entry => kusdUserBalance = Promise.resolve(entry.balance));


export const getDeploymentsFromTzkt = (factory=MARKET_FACTORY_ADDR) => {
    const ts = get(deploymentsTimestamp);
    return fetch(`${TZKT_URL}/accounts/${factory}/operations?type=origination${ts !== undefined ? "&timestamp.gt=" + ts : ""}&limit=200&sort=1`)
    .then(r => r.json())
    .then(ops => {
        if (ops[0]?.timestamp !== undefined) {
            deploymentsTimestamp.set(ops[0]?.timestamp);
        }
        return ops;
    })
    .then(ops => ops.filter(op => op.sender.address === factory)) // exclude the origination of the factory itself
    .then(ops => ops.map(op => op.originatedContract.address))
    .then(x => {console.log("getDeploymentsFromTzkt: ", x); return x;});
};

// old, uses BCD
// export const getContracts = (exampleMarket=EXAMPLE_MARKET_ADDR) => {
//     return fetch(`https://api.better-call.dev/v1/contract/florencenet/${exampleMarket}/same`)
//     .then(r => r.text())
//     .then(JSON.parse)
//     .then(same => same.contracts.map(c => c.address))
//     .then(x => {console.log("getContracts: ", x); return x});
// };

// old, uses taquito
// export const fmtContracts = addresses => {
//     return addresses.map(address => {
//         return Promise.resolve(tt)
//         .then(t => t.contract.at(address))
//         .then(c => c.storage())
//         .then(async s => {
//             const ts = new Date((await (await tt).rpc.getBlockHeader()).timestamp).valueOf();
            
//             const target_price = s.target_price.toString();
//             const asset = s.asset;
//             const bidding_end = new Date(s.bidding_end).valueOf();
//             const trading_end = new Date(s.trading_end).valueOf();
//             const phase = (ts === 0) ? "loading" : (ts < bidding_end) ? "bidding" : (ts < trading_end) ? "trading" : "maturity";
            
//             const bn_total_bids = s.total_long_bids.plus(s.total_short_bids);
//             const ls_ratio = `${s.total_long_bids.div(bn_total_bids).toFixed(2)} / ${s.total_short_bids.div(bn_total_bids).toFixed(2)}`;

//             const total_long_bids = s.total_long_bids.toString();
//             const total_short_bids = s.total_short_bids.toString();
//             const total_bids = bn_total_bids.toString();
            
//             return { address, target_price, asset, bidding_end, trading_end, phase, ls_ratio, total_bids, total_long_bids, total_short_bids };
//         });
//     })
//     .then(arr => Promise.all(arr))
//     .then(result => JSON.stringify(result, null, 2));
// };

// output of getContracts().then(contracts => fmtContracts(contracts)) -- use this for prerendering
export const markets = writable(posts);

// export const markets = writable([
//     {
//       "address": "KT1AqJPj1ZvzvFXtw3F17oSTo5Hi9E66Q92N",
//       "target_price": "111",
//       "asset": "BTC-USD",
//       "bidding_end": 1615676400000,
//       "trading_end": 1615762800000,
//       "phase": "trading",
//       "ls_ratio": "0.45 / 0.55",
//       "total_bids": "12221",
//       "total_long_bids": "5555",
//       "total_short_bids": "6666"
//     },
//     {
//       "address": "KT192vhzxe6bH9tTMXjvbzf4UFLfwcZ9KFPQ",
//       "target_price": "111",
//       "asset": "BTC-USD",
//       "bidding_end": 1615676400000,
//       "trading_end": 1615762800000,
//       "phase": "trading",
//       "ls_ratio": "0.45 / 0.55",
//       "total_bids": "1221",
//       "total_long_bids": "555",
//       "total_short_bids": "666"
//     },
//     {
//       "address": "KT1K3ejz7wTDg4BpXk3zDUJuehNd6ohEqe8V",
//       "target_price": "36000000000",
//       "asset": "BTC-USD",
//       "bidding_end": 1612036477000,
//       "trading_end": 1612040077000,
//       "phase": "maturity",
//       "ls_ratio": "0.46 / 0.54",
//       "total_bids": "26800000000000000000",
//       "total_long_bids": "12300000000000000000",
//       "total_short_bids": "14500000000000000000"
//     },
//     {
//       "address": "KT1UGoRki6JEkPKC9qo3xciRb9f3emtwBkqQ",
//       "target_price": "36000000000",
//       "asset": "BTC-USD",
//       "bidding_end": 1612036477000,
//       "trading_end": 1612040077000,
//       "phase": "maturity",
//       "ls_ratio": "0.46 / 0.54",
//       "total_bids": "26800000000000000000",
//       "total_long_bids": "12300000000000000000",
//       "total_short_bids": "14500000000000000000"
//     }
// ]);


export const marketRows = writable(new Map(get(markets).map(o => [o.address, o])));

// TzKT get storage if changed since last block
// https://api.florencenet.tzkt.io/v1/operations/transactions?target=KT18yj1c3sRnWNJbskosXveKBoZ4t2ik9SSH&level.gt=167129&sort.desc=level&limit=1&select=storage
// TzKT get current storage
// https://api.florencenet.tzkt.io/v1/contracts/KT18yj1c3sRnWNJbskosXveKBoZ4t2ik9SSH/storage
// 
// TzKT get current longs
// https://api.florencenet.tzkt.io/v1/contracts/KT18yj1c3sRnWNJbskosXveKBoZ4t2ik9SSH/bigmaps/long_bids/keys/tz1WB6v4YZ9Ze5Kwdw5x5daJLHyGQDWVD1Qo
// TzKT get current shorts
// https://api.florencenet.tzkt.io/v1/contracts/KT18yj1c3sRnWNJbskosXveKBoZ4t2ik9SSH/bigmaps/short_bids/keys/tz1WB6v4YZ9Ze5Kwdw5x5daJLHyGQDWVD1Qo
//
// ideally we would use TzKT websocket API
// TODO ask about bigmap key updates over websocket, there is no endpoint for them currently

export const spawnMarket = market => {
    console.log("market to spawn: ", market);

    const data = writable("loading");
    const getData = () => fetch(`${TZKT_URL}/contracts/${market.address}/storage`)
    .then(r => r.json())
    .then(s => {
        const kusdMul = new BigNumber(10).pow(new BigNumber(18));
        const kusdSmallest = new BigNumber(1);
        console.log('s: ', s);
        let d = {};
        d.address = market.address;
        d.target_price = new BigNumber(s.target_price);
        d.asset = s.asset;
        d.option_contract = s.option_contract;
        d.min_capital = new BigNumber(s.min_capital);
        d.skew_limit = new BigNumber(s.skew_limit);
        d.exercise_fee_multiplier = new BigNumber(s.exercise_fee_multiplier);
        d.cancel_fee_multiplier = new BigNumber(s.cancel_fee_multiplier);

        // TODO remove this
        // tests rendering of market state changes in UI
        // if(Math.random() < 0.5) {
        //     console.log("coinflip, doubling price");
        //     d.target_price = d.target_price.plus(d.target_price);
        // };

        // market_earnings come from cancelled bids
        d.market_earnings = new BigNumber(s.market_earnings);
        d.total_long_bids = new BigNumber(s.total_long_bids);
        d.total_short_bids = new BigNumber(s.total_short_bids);        
        d.total_bids = d.total_long_bids.plus(d.total_short_bids);
        const total_bids_earnings = d.total_bids.plus(d.market_earnings);
        // adding the smallest unit to account for precision loss
        // some dust is going to remain in the market, which is fine
        d.ls_ratio = `${d.total_long_bids.multipliedBy(kusdMul).div(total_bids_earnings).plus(kusdSmallest).div(kusdMul).toFixed(2)} / ${d.total_short_bids.multipliedBy(kusdMul).div(total_bids_earnings).plus(kusdSmallest).div(kusdMul).toFixed(2)}`;
        

        d.bidding_end = new Date(s.bidding_end).valueOf();
        d.trading_end = new Date(s.trading_end).valueOf();
        // console.log("s bidding_end: ", s.bidding_end);
        // console.log("s trading_end: ", s.trading_end);
        console.log("bidding_end: ", d.bidding_end);
        console.log("trading_end: ", d.trading_end);
        const ts = get(timestamp);
        d.timestamp = ts;
        console.log("timestamp: ", ts);
        // d.phase = (ts === 0) ? "loading" : (ts < d.bidding_end) ? "bidding" : (ts < d.trading_end) ? "trading" : "maturity";
        d.phase = (ts === 0) ? "loading" : (ts < d.bidding_end) ? 0 : (ts < d.trading_end) ? 1 : 2;
        
        return d;
    })
    .then(x => {console.log("getData of market ", market.address ," : ", x); return x})
    .then(d => {
        if (JSON.stringify(d) !== JSON.stringify(get(data))) {
            data.set(d);
            console.log("data ready: ", d);
        }
        if (JSON.stringify(d) !== JSON.stringify(get(marketRows).get(d.address))) {
            marketRows.update(m => m.set(d.address, d));
            console.log("rows ready: ", Array.from(get(marketRows).entries()));
        }
    }); 
    
    const longs = writable("loading");
    const getLongs = () => get(userAddr)
    .then(addr => fetch(`${TZKT_URL}/contracts/${market.address}/bigmaps/long_bids/keys/${addr}`))
    .then(r => r.text())
    .then(rt => rt ? JSON.parse(rt) : {})
    .then(entry => {
        if (entry?.active !== true) {
            return new BigNumber(0);
        } else if (entry?.value === undefined) {
            return new BigNumber(0);
        } else {
            return new BigNumber(entry.value);
        }
    })
    .then(x => {console.log("getLongs of market ", market.address ," : ", x); return x})
    .then(bids => {
        if (bids.toString() !== get(longs)?.toString())
            longs.set(bids);
    });

    const shorts = writable("loading");
    const getShorts = () => get(userAddr)
    .then(addr => fetch(`${TZKT_URL}/contracts/${market.address}/bigmaps/short_bids/keys/${addr}`))
    .then(r => r.text())
    .then(rt => rt ? JSON.parse(rt) : {})
    .then(entry => {
        if (entry?.active !== true) {
            return new BigNumber(0);
        } else if (entry?.value === undefined) {
            return new BigNumber(0);
        } else {
            return new BigNumber(entry.value);
        }
    })
    .then(x => {console.log("getShorts of market ", market.address ," : ", x); return x})
    .then(bids => {
        if (bids.toString() !== get(shorts)?.toString())
            shorts.set(bids);
    });


    return { stores: {data, longs, shorts}, getters: { getData, getLongs, getShorts } };
};

export const marketsMap = writable(new Map(get(markets).map(m => [m.address, spawnMarket(m)])));
export const addMarket = address => {
    const market = spawnMarket({ address });
    marketsMap.update(map => map.set(address, market));
    market.getters.getData();
    if (getBeaconAddress() !== undefined) {
        market.getters.getLongs();
        market.getters.getShorts();
    }
}